﻿public interface IRubikCubeRequired
{
    void OnNewRubikCubeFocused(RubikCubeInstance previous, RubikCubeInstance cube);
}